Projeto teste para Walmart.

# 1. Tecnologias
O projeto tem por base Spring (Boot, MVC, Security, Data), Hibernate com JPA, HSQLDB e Tomcat.

# 2. Arquitetura e Definições dos Objetos Java

## 2.1 Frameworks
Spring boot foi o framework de adoção pois facilitava a implementação e delivery rápido. A utilização do Hsqldb também foi uma escolha por 
facilidade, uma vez que a definição do modelo de dados não seria alterada independente do vendor de banco de dados. Além disso, a utilização
de Repository do SpringData foi a solução escolhida pois a visão do projeto é ser simplista e fornecer somente os serviços de melhor rota e,
mesmo assim, ser expansível para outros tratamentos de rota e somente isso.

## 2.2 Estruturação e padronização
Temos 3 pacotes principais no projeto, que representam uma aplicação 3 camadas:
A) br.com.jar.kiki.services: Serviços consumíveis
a) br.com.jar.kiki.business: Onde ficam compreendidas todas as classes que se referem à regras de negócio e são elas responsáveis em expor a 
camada de dados. Aqui, a linguagem de métodos e classes são totalmente voltadas ao negócio, podendo ser diferente a de acesso aos dados. 
Classes de Serviço são específicas para determinada atividade e BusinessObjects são mais crud.
C) br.com.jar.kiki.data: Acesso aos dados da aplicação.

# 3. Rodando a aplicação
## 3.1 Java
Espera-se que ele esteja instalado e configurado corretamente no classpath :)

# 3.2 HSQLDB
## 3.2.1 Download componente
Baixar o pacote disponivel no site do HSQLDB, versão utilizada '2.3.3'. Descompactá-lo na pasta que chamaremos agora de $HSQL_HOME.
## 3.2.2 Iniciando server
Rodar no prompt de comando as seguintes instruções:
% cd $HSQL_HOME\bin
% java -cp ../lib/hsqldb.jar org.hsqldb.Server -database.0 file:kikidb -dbname.0 kikidb
## 3.2.3 Teste de conexão
Prompt:
% cd $HSQL_HOME\lib
% javaw -jar hsqldb.jar
% exit

Na janela que abrir, configure da seguinte maneira:
- Type: HSQL Database Engine In-Memory
- Driver: org.hsqldb.jdbcDriver
- URL: jdbc:hsqldb:hsql://localhost/kikidb
- User: sa
- Password: <branco>

## 3.3 Modelo de dados
    Não se preocupe, na subida do sistema ele irá criar as tabelas

##3.4 Aplicação
Rodar pelo Maven, abrir prompt de comando na pasta do projeto:
% mvn spring-boot:run
    