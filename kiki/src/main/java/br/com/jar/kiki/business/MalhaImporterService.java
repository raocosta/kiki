package br.com.jar.kiki.business;

import java.text.MessageFormat;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.jar.kiki.business.crud.MalhaLogisticaBO;
import br.com.jar.kiki.business.exception.BusinessException;
import br.com.jar.kiki.data.entities.MalhaLogistica;
import br.com.jar.kiki.data.entities.MalhaLogistica.PK;
import br.com.jar.kiki.to.MalhaImportTO;
import br.com.jar.kiki.to.RotaImportTO;

/**
 * Esse servico tem como tarefa principal importar uma malha logistica e persisti-la. Baseado no mapa informado, retira
 * todos os registros referente a esse mapa e insere todos os dados informados. Pode também apenas atualizar o mapa jah
 * existente, adicionando os dados informados.
 * 
 * @author Raphael Costa
 *
 */
@Service
public class MalhaImporterService {

	private static final Log LOG = LogFactory.getLog(MalhaImporterService.class);

	@Resource
	private MalhaLogisticaBO malhaLogisticaBO;

	public MalhaImporterService() {
	}

	public void adicionarMalha(MalhaImportTO malha) throws BusinessException {
		inserirMalha(malha, true);
	}

	public void importarMalha(MalhaImportTO malha) throws BusinessException {
		inserirMalha(malha, false);
	}

	public void inserirMalha(MalhaImportTO malha, boolean update) throws BusinessException {
		LOG.debug("Iniciando processo de importacao de malha...");
		validarImportacao(malha);
		if (!update) {
			preparaImportacao(malha);
		}
		persistirMalha(malha);
	}

	private void validarImportacao(MalhaImportTO malha) throws BusinessException {
		if (malha == null) {
			throw new BusinessException("malhaImporterService.malha.null");
		}

		if (StringUtils.isEmpty(malha.getMapa())) {
			throw new BusinessException("malhaImporterService.malha.mapa.null");
		}

		if (CollectionUtils.isEmpty(malha.getRotas())) {
			throw new BusinessException("malhaImporterService.malha.rotas.null");
		}
	}

	private void preparaImportacao(MalhaImportTO malha) throws BusinessException {
		final String mapa = malha.getMapa();
		final int count;
		try {
			count = malhaLogisticaBO.deleteByNomMapa(mapa);
		} catch (Exception e) {
			throw new BusinessException("malhaImporterService.delete.error", e);
		}

		LOG.debug(MessageFormat.format("Malha {0} foram DELETADOS {1} registros", malha.getMapa(), count));
	}

	private void persistirMalha(MalhaImportTO malha) throws BusinessException {
		try {
			final List<RotaImportTO> rotas = malha.getRotas();
			for (RotaImportTO rota : rotas) {
				final MalhaLogistica entityIda = buildEntityFromImportsTOIda(malha.getMapa(), rota);
				malhaLogisticaBO.insert(entityIda);

				final MalhaLogistica entityVolta = buildEntityFromImportsTOVolta(malha.getMapa(), rota);
				malhaLogisticaBO.insert(entityVolta);

			}
		} catch (Exception e) {
			throw new BusinessException("malhaImporterService.persist.error", e);
		}

		LOG.debug(MessageFormat.format("Malha {0} foram INSERIDOS {1} registros", malha.getMapa(), (malha.getRotas().size() * 2)));
	}

	private MalhaLogistica buildEntityFromImportsTOIda(String mapa, RotaImportTO rota) {
		return buildEntityFromImportsTO(mapa, rota, false);
	}

	private MalhaLogistica buildEntityFromImportsTOVolta(String mapa, RotaImportTO rota) {
		return buildEntityFromImportsTO(mapa, rota, true);
	}

	/**
	 * @param mapa
	 * @param rota
	 * @param inverter
	 * @return
	 */
	private MalhaLogistica buildEntityFromImportsTO(String mapa, RotaImportTO rota, boolean inverter) {
		final MalhaLogistica ret = new MalhaLogistica();

		ret.setVlrDistancia(rota.getDistancia());
		final PK pk;
		if (inverter) {
			pk = new PK(mapa, rota.getDestino(), rota.getOrigem());
		} else {
			pk = new PK(mapa, rota.getOrigem(), rota.getDestino());
		}
		ret.setPk(pk);

		return ret;
	}

}
