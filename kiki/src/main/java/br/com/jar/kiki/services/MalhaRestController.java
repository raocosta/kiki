package br.com.jar.kiki.services;

import java.math.BigDecimal;
import java.text.MessageFormat;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.jar.kiki.business.MalhaImporterService;
import br.com.jar.kiki.business.MelhorRotaService;
import br.com.jar.kiki.to.MalhaImportTO;
import br.com.jar.kiki.to.MelhorRotaTO;

@RestController
@RequestMapping("/malha")
public class MalhaRestController {

	private static final Log LOG = LogFactory.getLog(MalhaRestController.class);

	@Resource
	private MalhaImporterService malhaImporterService;

	@Resource
	private MelhorRotaService melhorRotaService;

	@RequestMapping(value = "/import", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String importMalha(@RequestBody MalhaImportTO malha) {
		try {
			malhaImporterService.importarMalha(malha);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return MessageFormat.format("{'result':'NOK', 'message':'{0}'}", e.getMessage());
		}

		return "{'result':'OK'}";
	}

	@RequestMapping(value = "/import", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String patchMalha(@RequestBody MalhaImportTO malha) {
		try {
			malhaImporterService.adicionarMalha(malha);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return MessageFormat.format("{'result':'NOK', 'message':'{0}'}", e.getMessage());
		}

		return "{'result':'OK'}";
	}

	@RequestMapping(value = "/melhorRota/{mapa}/{origem}/{destino}/{autonomia}/{custo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MelhorRotaTO> melhorRota(@PathVariable String mapa, @PathVariable String origem, @PathVariable String destino, @PathVariable BigDecimal autonomia,
			@PathVariable BigDecimal custo) {

		final MelhorRotaTO melhorRota;
		try {
			melhorRota = melhorRotaService.findMelhorRota(mapa, origem, destino, autonomia, custo);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return new ResponseEntity<MelhorRotaTO>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<MelhorRotaTO>(melhorRota, HttpStatus.OK);
	}

}