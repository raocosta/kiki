Projeto teste para Walmart.

# 1. Tecnologias
O projeto tem por base Spring (Boot, MVC, Security, Data), Hibernate com JPA, HSQLDB e Tomcat.

# 2. Arquitetura e Definições dos Objetos Java

## 2.1 Frameworks
Spring boot foi o framework de adoção pois facilitava a implementação e delivery rápido. A utilização do Hsqldb também foi uma escolha por 
facilidade, uma vez que a definição do modelo de dados não seria alterada independente do vendor de banco de dados. Além disso, a utilização
de Repository do SpringData foi a solução escolhida pois a visão do projeto é ser simplista e fornecer somente os serviços de melhor rota e,
mesmo assim, ser expansível para outros tratamentos de rota e somente isso.

## 2.2 Estruturação e padronização
Temos 3 pacotes principais no projeto, que representam uma aplicação 3 camadas:
    a) br.com.jar.kiki.services: Serviços consumíveis
    b) br.com.jar.kiki.business: Onde ficam compreendidas todas as classes que se referem à regras de negócio e são elas responsáveis em expor a 
camada de dados. Aqui, a linguagem de métodos e classes são totalmente voltadas ao negócio, podendo ser diferente a de acesso aos dados. 
Classes de Serviço são específicas para determinada atividade e BusinessObjects são mais crud.
    c) br.com.jar.kiki.data: Acesso aos dados da aplicação.

# 3. Rodando a aplicação
## 3.1 Java
Espera-se que ele esteja instalado e configurado corretamente no classpath :)

# 3.2 HSQLDB

## 3.2.1 Download componente
Baixar o pacote disponivel no site do HSQLDB, versão utilizada '2.3.3'. Descompactá-lo na pasta que chamaremos agora de $HSQL_HOME.

## 3.2.2 Iniciando server
Rodar no prompt de comando as seguintes instruções:

* cd $HSQL_HOME\bin
* java -cp ../lib/hsqldb.jar org.hsqldb.Server -database.0 file:kikidb -dbname.0 kikidb

## 3.2.3 Teste de conexão
Prompt:
* cd $HSQL_HOME\lib
* javaw -jar hsqldb.jar
* exit

Na janela que abrir, configure da seguinte maneira:

* Type: HSQL Database Engine In-Memory
* Driver: org.hsqldb.jdbcDriver
* URL: jdbc:hsqldb:hsql://localhost/kikidb
* User: sa
* Password: <branco>

## 3.3 Modelo de dados
    Não se preocupe, na subida do sistema ele irá criar as tabelas

##3.4 Aplicação
Rodar pelo Maven, abrir prompt de comando na pasta do projeto:

* mvn spring-boot:run

# 4. Acessando o sistema
## 4.1 Import de malha
Esse serviço recebe toda a malha especificada para um mapa e a persiste. Qualquer informação anterior referente ao mapa informado é deletada.

### 4.1.1 Requisição
Utilize sua ferramenta favorita para requisições HTTP, sugiro a utilização da extension do chrome [PostMan](https://chrome.google.com/webstore/detail/postman-rest-client/fdmmgilgnpjigdojojpjoooidkmcomcm?utm_source=chrome-app-launcher-info-dialog)

Configuração da requisição:

- url: http://localhost:8080/malha/import
- method: POST
- header: Content-Type:application/json
- body: json no seguinte formato:
    - {"mapa":"SP", "rotas":[
{"origem":"A", "destino":"B", "distancia":"10"},
{"origem":"B", "destino":"D", "distancia":"15"},
{"origem":"A", "destino":"C", "distancia":"20"},
{"origem":"C", "destino":"D", "distancia":"30"},
{"origem":"B", "destino":"E", "distancia":"50"},
{"origem":"D", "destino":"E", "distancia":"30"}]
}

### 4.1.2 Resultados possíveis
Retorno json que indica sucesso ou falha:

    Sucesso (http 200): {'result':'OK'}

    Falha (http 200): {'result':'NOK', 'mensagem':'$MensagemDeErroDoServidor'}

    Falha Inesperada (http 500): {
    "timestamp": 1444313028292,
    "status": 500,
    "error": "Internal Server Error",
    "exception": "java.lang.IllegalArgumentException",
    "message": "can't parse argument number: 'result':'NOK'",
    "path": "/malha/import"
}

## 4.2 Buscar melhor rota
Através dos pontos de origem e destino informado calcula a melhor rota, custo e distância totais. A busca de melhor rota vai até 4 níveis, espera-se que a malha informada preveja pontos extremamente distantes e informe pontos intermediários entre tais destinos. Exemplo: do ponto A ao K, ao invés de traçar uma rota A->B->C...J->K, a malha suportaria pontos intermediários como A->D->H->J.

### 4.2.1 Requisição
Configuração da requisição:

- url: http://localhost:8080/malha/melhorRota/{mapa}/{origem}/{destino}/{autonomia}/{custoLitro}/
- method: GET

### 4.2.2 Resultados possíveis
    Sucesso (http 200 e retorno json): 
    {
    "mapa": "SP",
    "origem": "A",
    "destino": "B",
    "pontos": [
        "A",
        "B"
    ],
    "distanciaTotal": 10,
    "custoTotal": 2.9,
    "rota": "A -> B"}


    Falha (http 500): **sem json**

## 4.3 Update de malha
Esse serviço disponibiliza a atualização da malha, sem que delete nenhuma rota já definida na malha.

### 4.3.1 Requisição
Configuração da requisição:

- url: http://localhost:8080/malha/import
- method: PATCH
- header: Content-Type:application/json
- body: json no seguinte formato:
    - {"mapa":"SP", "rotas":[
{"origem":"A", "destino":"D", "distancia":"10"},
{"origem":"D", "destino":"H", "distancia":"15"},
{"origem":"H", "destino":"J", "distancia":"20"}]
}

### 4.3.2 Resultados possíveis
Retorno json que indica sucesso ou falha:

    Sucesso (http 200): {'result':'OK'}

    Falha (http 200): {'result':'NOK', 'mensagem':'$MensagemDeErroDoServidor'}

    Falha Inesperada (http 500): {
    "timestamp": 1444313028292,
    "status": 500,
    "error": "Internal Server Error",
    "exception": "java.lang.IllegalArgumentException",
    "message": "can't parse argument number: 'result':'NOK'",
    "path": "/malha/import"}